package inheritance;

public class Bookstore {
    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book("The Rise","Anna");
        books[1] = new ElectronicBook("The Slant", "Brent", 452);
        books[2] = new Book("The Fall","Emily");
        books[3] = new ElectronicBook("The Side", "Brandon", 378);
        books[4] = new ElectronicBook("The Back", "Braxton", 222);

        for (int i = 0; i < books.length; i++) {
            System.out.println(books[i]);
        }
    }
}
