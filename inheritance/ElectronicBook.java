package inheritance;

public class ElectronicBook extends Book {
    int numberBytes;

    public ElectronicBook(String title, String author, int numberBytes) {
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public int getNumberBytes() {
        return numberBytes;
    }

    public String toString() {
        String string = super.toString();
        return string + "\nnumberBytes: " + this.numberBytes;
    }
}